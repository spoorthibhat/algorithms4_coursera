package com.week3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TSPHeuristic {

	/**
	 * @param args
	 */
	private Map<Integer, Point> cities;
	private class Point{
		
		private double x;
		private double y;
		
		public Point(double x, double y){
			
			this.x = x;
			this.y = y;
		}
	}
	
	public TSPHeuristic(FileReader file) throws IOException{
		
		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine();
		
		cities = new HashMap<Integer, TSPHeuristic.Point>();
		while((line = reader.readLine()) != null){
			String[] row = line.trim().split(" ");
			cities.put(Integer.parseInt(row[0]), new Point(Double.parseDouble(row[1]),Double.parseDouble(row[2])));
		}
		
	}
	
	public void costOfTravel(){
		
		double cost = 0;;
		
		Point currentCityPoint = cities.get(1);
		Point startCity = currentCityPoint; // storing the start city value
		cities.remove(1);
		while(!cities.isEmpty()){
			
			double minDistance = Double.MAX_VALUE;
			Point closestCity = null;
			int closestCityIndex = 0;
			for (Map.Entry<Integer, Point> entry : cities.entrySet()) {
				
				Point reachableCity = entry.getValue();
				double distance = euclideanSquare(currentCityPoint, reachableCity);
				
				if(minDistance > distance ){
					
					minDistance = distance;
					closestCity = reachableCity;
					closestCityIndex = entry.getKey();
				}else if(minDistance == distance){ // breaking the ties by index
					
					if(closestCityIndex > entry.getKey()){
						closestCityIndex = entry.getKey();
						closestCity = reachableCity;
					}
				}
			    
			}
//			System.out.print(closestCityIndex);
//			System.out.print(" ");
			currentCityPoint = closestCity;
			cities.remove(closestCityIndex);
			cost += Math.sqrt(minDistance);
			if(cities.isEmpty()){ // get back to where he started
				cost += Math.sqrt(euclideanSquare(closestCity, startCity));
			}
		}
		
		//System.out.println();
		//System.out.println(cost);
		System.out.println((int)Math.floor(cost));
	}
	
	private double euclideanSquare(Point one, Point two){
		
		double firstPart = Math.pow(one.x - two.x, 2.0);
		double secPart = Math.pow(one.y - two.y, 2.0);
		
		return firstPart + secPart;
		
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms4/Week3/NN.txt");
		
		TSPHeuristic tspH = new TSPHeuristic(file);
		tspH.costOfTravel();
		// For NN.txt (This was the input file given for assignment), the answer is 1203406
	}

}
