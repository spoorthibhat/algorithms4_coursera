package com.week4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Random;

public class TwoSat {

	
	private ArrayList<Clause> requiredClauses;
	
	
	private class Clause{
		
		private String variable1;
		private String variable2;
		
		Clause(String var1, String var2){
			
			variable1 = var1;
			variable2 = var2;
		}
		
		@Override
		public int hashCode(){
			
			return Objects.hash(variable1,variable2);
		}
		
		@Override
		public boolean equals(Object o){

			if(o == null){
				return false;
			}
			if(!(o instanceof Clause)){
				return false;
			}

			if(o == this){
				return true;
			}
			Clause clause = (Clause)o;

			return (clause.variable1 == this.variable1 && clause.variable2 == this.variable2);
		}
	}
	
	public TwoSat(String file) throws IOException{
		
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		String line = reader.readLine();
		requiredClauses = new ArrayList<Clause>();
		ArrayList<Clause> fileInputClause = new ArrayList<Clause>();
		while((line = reader.readLine()) != null){
			
			String[] row = line.trim().split(" ");
			Clause inpclause = new Clause(row[0], row[1]);
			fileInputClause.add(inpclause);
		}
		// reading the file 1st time for finding the uniquely represented variables if present.
		
		
		HashSet<String> allVariables = reduction(fileInputClause,100);
		System.out.println("Reduction complete!");
		if(requiredClauses.size()>1){
		TwoSatRandomisedAlg(allVariables);
		}else{
			System.out.println(true);
		}
		
	}
	
	private HashSet<String> reduction(ArrayList<Clause> currentList, int reductionLimit){
		
		ArrayList<Clause> nextList = new ArrayList<TwoSat.Clause>();
		HashSet<String> variablesSet = null; 
		for(int i = 0; i < reductionLimit; i++){
			
			HashSet<String> negatedVaribles = new HashSet<String>();
			HashSet<String> unnegatedVariables = new HashSet<String>();
			
			for(Clause clause: currentList){
				
				String var1 = clause.variable1;
				String var2 = clause.variable2;
				if(var1.startsWith("-")){
					var1 = var1.substring(1);
					negatedVaribles.add(var1);
				}else{
					unnegatedVariables.add(var1);
				}
				
				if(var2.startsWith("-")){
					var2 = var2.substring(1);
					negatedVaribles.add(var2);
				}else{
					unnegatedVariables.add(var2);
				}
				
			}
			
			for(Clause eachClause : currentList){
				boolean check = true;
				String variable1 = eachClause.variable1;
				String variable2 = eachClause.variable2;
				if(variable1.startsWith("-")){
					variable1 = variable1.substring(1);
				}
				if(variable2.startsWith("-")){
					variable2 = variable2.substring(1);
				}
				if(!negatedVaribles.contains(variable1) || !unnegatedVariables.contains(variable1) || !negatedVaribles.contains(variable2) || !unnegatedVariables.contains(variable2)){
					check = false;
				}
				
				if(check){
					nextList.add(eachClause);
				}
			}
			currentList.clear();
			currentList = new ArrayList<TwoSat.Clause>(nextList);
			nextList.clear();
			if(i == reductionLimit-1){
				variablesSet = new HashSet<String>(unnegatedVariables);
			}
		}
		requiredClauses = currentList;
		return variablesSet;
	}
	private void TwoSatRandomisedAlg(HashSet<String> variables){
		
		
		int outerLoopLimit = (int) (Math.log(requiredClauses.size())/Math.log(2));
		boolean isSatified = true;
		for(int i = 0; i <= (outerLoopLimit); i++){
			
			HashMap<String, Boolean> assignment = new HashMap<String, Boolean>(randomAssignmentGenerator(variables));
			
			int innerLimit = (int)(2*Math.pow(requiredClauses.size(), 2.0));
			for( int j = 0; j <= innerLimit; j++){
				ArrayList<Clause> unsatisfiedClauses = new ArrayList<Clause>();
				
				// check if current assignment satisfies all clauses
				for( Clause eachClause : requiredClauses){
					
					String var1 = eachClause.variable1;
					String var2 = eachClause.variable2;
					
					boolean firstPart;
					boolean secPart;
					if(var1.startsWith("-")){
						var1 = var1.substring(1);
						firstPart = !(assignment.get(var1));
					}else{
						firstPart = assignment.get(var1);
					}
					if(var2.startsWith("-")){
						var2 = var2.substring(1);
						secPart = !(assignment.get(var2));
					}else{
						secPart = assignment.get(var2);
					}
					
					if(!(firstPart || secPart)){
						unsatisfiedClauses.add(eachClause);
						isSatified = false; // not sure if needed
					}
					
				}
				if(unsatisfiedClauses.size() == 0){
					isSatified = true;
					break;
				}else{
				
					Random rand = new Random();
					int randomNum = rand.nextInt(unsatisfiedClauses.size());
					Clause randomClause = unsatisfiedClauses.get(randomNum);
					String v1 = randomClause.variable1;
					String v2 = randomClause.variable2;
					if(v1.startsWith("-")){
						v1 = v1.substring(1);
					}
					if(v2.startsWith("-")){
						v2 = v2.substring(1);
					}
					if(Math.random() > 0.5){
						boolean prevAssigment = assignment.get(v1);
						assignment.put(v1, !prevAssigment);
					}else{
						boolean prevAssigment = assignment.get(v2);
						assignment.put(v2, !prevAssigment);
					}
			}
				
			}
			if(isSatified){
				System.out.println(isSatified);
				break;
			}
			
		}
		System.out.println(isSatified);
	}
	private HashMap<String,Boolean> randomAssignmentGenerator(HashSet<String> allVariables){
		
		boolean value;
		HashMap<String,Boolean> assigment = new HashMap<String, Boolean>();
		
		for(String eachVariable: allVariables){
			if(Math.random() > 0.5){
				value = true;
			}else{
				value = false;
			}
			assigment.put(eachVariable, value);
		}
		
		return assigment;
	}
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub

		String file = "/Users/spoor/Desktop/Algorithms_assignment/Algorithms4/Week4/2Sat6.txt";
		//FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms4/Week4/TwoSat_inp4_false.txt");
		
		TwoSat twosat = new TwoSat(file);
		// Used reduction limit as 100 for very huge inputs. i.e.,for input size > 100000
		// 2Sat1.txt = true
		// 2Sat2.txt = false
		// 2Sat3.txt = true
		// 2Sat4.txt = true
		// 2Sat5.txt = false
		// 2Sat6.txt = false
	}

}
