package com.week2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class TravellingSalesmanProblem {

	/**
	 * @param args
	 */
	private int numOfNodes;

	/**
	 * its an array that stores the list of hashsets(subsets) in the position
	 * correspoinding to the subsets size.
	 */
	private ArrayList<HashSet<Point>>[] subsets; 
	private Map<ArrayList<Point>, Double> edges;
	private HashMap<Integer, Point> points;
	//private HashMap<TSPtour,Double> helperTour;

	public class Point{

		private double x;
		private double y;

		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
	}
	private class TSPtour{

		private HashSet<Point> path;
		private Point endPoint;

		TSPtour(HashSet<Point> path,Point endPoint){

			this.path = path;
			this.endPoint = endPoint;
		}


		@Override
		public int hashCode() {
			//TODO - We might have to think about re-writing this whole logic.
			int ret = Double.valueOf(endPoint.x).hashCode() 
			+ 3* Double.valueOf(endPoint.y).hashCode();
			for (Point p: this.path) {
				ret = ret + 5* Double.valueOf(p.x).hashCode() + 7*Double.valueOf(p.y).hashCode();
			}
			return ret;
		}

		@Override
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}

			if (! (TSPtour.class.isAssignableFrom(o.getClass()))) {
				return false;
			}

			final TSPtour p = (TSPtour) o;

			if (p.endPoint.x == this.endPoint.x && p.endPoint.y == this.endPoint.y) {
				if (p.path.containsAll(this.path) && this.path.containsAll(p.path)) {
					return true;
				}
			}
			return false;
		}


		public String stringify() {

			String ret = "";
			for (Point p : this.path) {
				ret = ret + "("+p.x+","+p.y+")";
			}

			return "X="+ this.endPoint.x + " Y=" +this.endPoint.y + " and HashSet:" + ret;
		}
	}

	public TravellingSalesmanProblem(FileReader file)throws IOException{

		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine().trim();
		numOfNodes = Integer.parseInt(line);
		subsets = new ArrayList[numOfNodes];
		edges = new HashMap<ArrayList<Point>, Double>();
		//helperTour = new HashMap<TSPtour, Double>();

		for(int i = 0; i < numOfNodes; i++){
			subsets[i] = new ArrayList<HashSet<Point>>();
		}
		points = new HashMap<Integer,Point>();
		int idx = 0;
		while((line = reader.readLine()) != null){
			String[] row = line.trim().split(" ");
			Point point = new Point(Double.parseDouble(row[0]), Double.parseDouble(row[1]));
			points.put(idx,point);
			idx++;
		}

		generateSubsets(points);
	}

	private void generateSubsets(HashMap<Integer, Point> inputPoints){


		for(int start = 1; start < Math.pow(2, (numOfNodes - 1) * 1.0); start++){

			String subsetString = Integer.toBinaryString(start);
			subsetString = String.format("%1$" + (numOfNodes - 1)  + "s", subsetString).replace(' ', '0'); // changed
			char[] bits = subsetString.toCharArray();
			HashSet<Point> subset = new HashSet<Point>();
			for(int i = 0; i < bits.length; i++){

				if(bits[i] == '1'){
					subset.add(inputPoints.get(i+1)); // CHANGED
				}
			}
			subsets[subset.size()].add(subset);
			//System.out.println("Subset size : " +subset.size());
			//start++;
		}

	}

	public double minimumCostTour(){

		double minCostTourValue = Double.MAX_VALUE;
		int i = 0; 
		HashMap<TSPtour,Double> helperTour = new HashMap<TSPtour, Double>();
		// HashMap<TSPtour, Point> parentTracker = new HashMap<TSPtour, Point>();
		Point startPoint = points.get(i);
		// Point currentEndPrev = null;

		for(int size = 1 ; size <= numOfNodes -1; size++){
			//System.out.println("Subset size : " +size);
			ArrayList<HashSet<Point>> allsubsets = subsets[size];
			for( HashSet<Point> eachSubset : allsubsets){
				if(eachSubset.contains(startPoint)){
					continue;
				}

				for(int j = 0; j < numOfNodes; j++){
					if(j != i){

						if(size == numOfNodes - 1){
							// this was because once the subset size reaches its maximum, 
							// we just want to return back to starting point completing the loop
							// but we do not want this (j = i) to happen for other subset sizes as 
							// there is still scope to find a subset that does not contain the to node.
							j = i;
						}
						Point toPoint = points.get(j);
						if(eachSubset.contains(toPoint)){ // compute to all points except the ones present in that subset
							continue;
						}

						double minTourDistance = Double.MAX_VALUE;


						TSPtour tourTaken = new TSPtour(eachSubset, toPoint);
						HashSet<Point> copyOfSubset = new HashSet<TravellingSalesmanProblem.Point>(eachSubset);
						for(Point x : eachSubset){
							double cost = euclideanDistance(x, toPoint);
							if(size == 1){

								minTourDistance = cost + euclideanDistance(startPoint, x);
								break;
							}
							copyOfSubset.remove(x);
							TSPtour searchingTour = new TSPtour(copyOfSubset, x);
							//System.out.println("searching hashcode:" + searchingTour.hashCode());
							double sec = helperTour.get(searchingTour);
							double calculatedTour = cost + sec;
							if(calculatedTour < minTourDistance){
								minTourDistance = calculatedTour;
								// currentEndPrev = x;

							}
							//minTourDistance = Math.min((cost + sec), minTourDistance);
							copyOfSubset.add(x);

						}
						//parentTracker.put(tourTaken, currentEndPrev);
						if(j == i){

							//								chosenStart = points.get(i);
							//								previosPointBeforeEnd = currentEndPrev;
							minCostTourValue = minTourDistance;
							//trackingPath(parentTracker, tourTaken);
							break;
						}
						//System.out.println("HashCode for inserting" + tourTaken.stringify() + "is " + tourTaken.hashCode());
						helperTour.put(tourTaken, minTourDistance);

					}

				}
			}
			//}
		}

		return minCostTourValue;
	}

	public double euclideanDistance(Point one, Point two){
		ArrayList<Point> edge = new ArrayList<Point>();
		edge.add(one);
		edge.add(two);

		if(!edge.contains(edge)){
			double firstPart = Math.pow((one.x - two.x), 2.0);
			double secPart = Math.pow((one.y - two.y), 2.0);

			Double result = Math.sqrt(firstPart + secPart);
			edges.put(edge, result);
			return result;
		}else{
			return edges.get(edge);
		}
	}

	/**
	 * This was only for my own debugging of the problem!!
	 * @param parentTracker - hashmap that stores the parent points
	 * @param lastTour - the whole tour the salesman takes just before coming back to start point
	 */
	private void trackingPath(HashMap<TSPtour, Point> parentTracker,TSPtour lastTour){

		//Point lastStop = lastTour.endPoint;

		while(lastTour.path.size() > 1){
			// misses out just one point
			//			if(lastTour.path.size() == 1){
			//				Point prevStop = parentTracker.get(lastTour);
			//				System.out.print("(" +prevStop.x + "," +prevStop.y+")<-");
			//				break;
			//			}
			Point prevStop = parentTracker.get(lastTour);
			System.out.print("(" +prevStop.x + "," +prevStop.y+")<-");
			HashSet<Point> remainingSet = lastTour.path;
			remainingSet.remove(prevStop);
			TSPtour newTour = new TSPtour(remainingSet, prevStop);

			trackingPath(parentTracker, newTour);
		}


	}

	/*
	 * The input has been divided into left half and right half due to the huge input file.
	 * 
	 */
	public static void main(String[] args) throws IOException {

		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms4/Week2/tsp_leftHalf.txt");
		TravellingSalesmanProblem tsp = new TravellingSalesmanProblem(file);
		double minValueLeft = tsp.minimumCostTour();
		System.out.println("Left half minimum cost tour : " +minValueLeft);

		FileReader fileRight = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms4/Week2/tsp_rightHalf.txt");
		TravellingSalesmanProblem tspRight = new TravellingSalesmanProblem(fileRight);
		double minValueRight = tspRight.minimumCostTour();
		System.out.println("Right half minimum cost tour: " +minValueRight);


		double deletingCost = 2 * tsp.euclideanDistance(tsp.points.get(11), tsp.points.get(12));
		double minValue = minValueLeft + minValueRight - deletingCost ;
		System.out.println("Total minCost tour : " + (int)Math.floor(minValue));
	}

	// Expected result: 
	// 14662.0046407879 for left half
	// 14409.202165641733 for right half
	// 26442 is the final mincost tour


}
