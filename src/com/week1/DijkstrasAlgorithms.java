package com.week1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import com.week1.AllPairShortestPaths_Johnsons.Edge;


public class DijkstrasAlgorithms {

	/**
	 * @param args
	 */

	private ArrayList<AllPairShortestPaths_Johnsons.Edge>[] inputGraph;
	private int numOfVertices;
	private boolean[] isAdded;
	private int[] greedyScoreOFVertex;
	private List<Integer> exploredVertices;
	private int minDistance; 

	public DijkstrasAlgorithms(ArrayList<AllPairShortestPaths_Johnsons.Edge>[] input ,int numOfNodes){

		inputGraph = input;
		numOfVertices = numOfNodes;
		isAdded = new boolean[numOfVertices + 1];
		greedyScoreOFVertex = new int[numOfVertices+1];
		minDistance = Integer.MAX_VALUE;
		exploredVertices = new ArrayList<Integer>();
		for(int i = 0; i <= numOfVertices; i++){

			greedyScoreOFVertex[i] = 0;
			isAdded[i] = false;
		}


	}

	public int dijkstraShortestDistance(int sourceVertex, int[][] pParameterOfVertex){

		PriorityQueue<Integer> minPQ = new PriorityQueue<Integer>(1,scoreComparator());
		exploredVertices.add(sourceVertex);
		isAdded[sourceVertex] = true;
		int recentlyAdded = sourceVertex;
		
		while(exploredVertices.size() != numOfVertices ){

			minPQ = addReachableVertices(minPQ, recentlyAdded);
			if(minPQ.isEmpty()){
				break;
			}
			recentlyAdded = getRecent(minPQ);
			int originalDistance = greedyScoreOFVertex[recentlyAdded] - pParameterOfVertex[1][sourceVertex] + pParameterOfVertex[1][recentlyAdded];
			if(originalDistance < minDistance){
				minDistance = originalDistance;
			}
		}

		return minDistance;
	}

	public PriorityQueue<Integer> addReachableVertices(PriorityQueue<Integer> minPQ, int recentlyAddedVertex){

		for(Edge edge : inputGraph[recentlyAddedVertex]){
			if(!isAdded[edge.otherNode]){
				if(!minPQ.contains(edge.otherNode)){
					greedyScoreOFVertex[edge.otherNode] = greedyScoreOFVertex[recentlyAddedVertex] + edge.edgeLength;

					minPQ.add(edge.otherNode);
				}else{
					int newlyComputedScore = greedyScoreOFVertex[recentlyAddedVertex] + edge.edgeLength;
					if(newlyComputedScore < greedyScoreOFVertex[edge.otherNode]){
						minPQ.remove(edge.otherNode);
						greedyScoreOFVertex[edge.otherNode] = newlyComputedScore;
						minPQ.add(edge.otherNode);
					}
				}
			}
		}
		return minPQ;
	}

	private int getRecent(PriorityQueue<Integer> minPQ){

		int toBeAddedNext = minPQ.poll();
		exploredVertices.add(toBeAddedNext);
		isAdded[toBeAddedNext] = true;
		return toBeAddedNext;
	}
	public Comparator<Integer> scoreComparator(){
		return new ScorePriority();
	}

	private class ScorePriority implements Comparator<Integer>{ // this is done because we want to add the vertex into the queue and not the edge length.

		public int compare(Integer vertex1, Integer vertex2){
			if(greedyScoreOFVertex[vertex1] > greedyScoreOFVertex[vertex2]){
				return +1;
			}else if (greedyScoreOFVertex[vertex1] < greedyScoreOFVertex[vertex2]){
				return -1;
			}else{
				return 0;
			}
		}


	}
}
