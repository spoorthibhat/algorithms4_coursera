package com.week1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


/**
 * This program implements johnson's algorithm to compute all pair shortest paths
 * If negative cost cycle is present, then the program detects it.
 * If not, it outputs the minimum of all the shortest paths.
 * @author Spoorthi Bhat
 *
 */
public class AllPairShortestPaths_Johnsons {

	/**
	 * @param args
	 */
	//private int[] helperParameterPOfVertex;
	private String inputFile;
	private int numOfNodes;
	private ArrayList<Edge>[] inputWithIncomingEdges ;
	private ArrayList<Edge>[] inputAfterReweighting; // contains outgoing edges for every vertex
	private int[][] pParameterOfVertex;
	/*
	 * This class defines either incoming or outgoing edge.
	 * The otherNode can be of tailNode or headNode depending on usage.
	 */
	public class Edge{
		
		public int otherNode;
		public int edgeLength;
		
		Edge(int otherNode, int edgeLength){
			
			this.edgeLength = edgeLength;
			this.otherNode = otherNode;
		}
	}
	
	public AllPairShortestPaths_Johnsons(String fileName) throws IOException{
		
		inputFile = fileName;
		FileReader file = new FileReader(inputFile);
		BufferedReader reader = new BufferedReader(file);
		String[] line = reader.readLine().trim().split(" ");
		numOfNodes = Integer.parseInt(line[0]);
		// helperParameterPOfVertex = new int[numOfNodes + 1];
		inputWithIncomingEdges = new ArrayList[numOfNodes + 1];
		pParameterOfVertex = new int[2][numOfNodes + 1];
		inputAfterReweighting = new ArrayList[numOfNodes + 1];
		
		for(int i = 0; i <= numOfNodes; i++){
			inputWithIncomingEdges[i] = new ArrayList<Edge>();
			inputAfterReweighting[i] = new ArrayList<Edge>();
			Edge edge = new Edge(0, 0);
			inputWithIncomingEdges[i].add(edge); // adding the extra source vertex for bellman ford algorithm to compute the shortest distances.
			pParameterOfVertex[0][i] = Integer.MAX_VALUE;
		}
		
	}
	
	public void shortest_ShortestDistanceComputation() throws IOException{
		
		boolean negative_cycle = bellmanFord();
		if(!negative_cycle){
		reweightingTechnique();
		int shortestOfAllShortestDistances = NDijkstras();
		
		System.out.println("The shortest of all shortest distances is : " +shortestOfAllShortestDistances);
		}
	}
	
	private boolean bellmanFord() throws IOException{
		FileReader file = new FileReader(inputFile);
		BufferedReader reader = new BufferedReader(file);
		
		String line = reader.readLine();
		
		while((line = reader.readLine()) != null){
			String[] row = line.trim().split(" ");
			int tailNode = Integer.parseInt(row[0]);
			int headNode = Integer.parseInt(row[1]);
			Edge incomingEdge = new Edge(tailNode, Integer.parseInt(row[2]));
			inputWithIncomingEdges[headNode].add(incomingEdge);
		}
		return pParameterOfVertexComputationBellman();
	}
	
	private int NDijkstras(){
		int shortestShortestDistance = Integer.MAX_VALUE;
		for(int source_vertex = 1; source_vertex <= numOfNodes; source_vertex++){
			
			DijkstrasAlgorithms dijkstra = new DijkstrasAlgorithms(inputAfterReweighting, numOfNodes);
			
			int minDistance = dijkstra.dijkstraShortestDistance(source_vertex, pParameterOfVertex);
			//  = min_edge.minDistance - pParameterOfVertex[1][min_edge.tailU] + pParameterOfVertex[1][min_edge.headV];
			
			if(minDistance < shortestShortestDistance){
				shortestShortestDistance = minDistance;
			}
		}
		return shortestShortestDistance;
	}                                                                                                                                                                                                                                                                                                                               
	private void reweightingTechnique() throws IOException{
		
		FileReader file = new FileReader(inputFile);
		BufferedReader reader = new BufferedReader(file);
		
		String line = reader.readLine();
		
		while((line = reader.readLine()) != null){
			
			String[] row = line.trim().split(" ");
			int tailNode = Integer.parseInt(row[0]);
			int headNode = Integer.parseInt(row[1]);
			int newEdgeLength = Integer.parseInt(row[2]) + pParameterOfVertex[1][tailNode] - pParameterOfVertex[1][headNode];
			Edge outgoingEdge = new Edge(headNode, newEdgeLength);
			inputAfterReweighting[tailNode].add(outgoingEdge);
		}
	}
	private boolean pParameterOfVertexComputationBellman(){
		
		pParameterOfVertex[0][0] = 0; // a[i][s] = 0;
		pParameterOfVertex[1][0] = 0;
		
		boolean negativeCycle = false;
		for(int i = 1; i <= numOfNodes ;i++){
			for(int vertex = 1 ; vertex <= numOfNodes; vertex++){
				
				if(i % 2 != 0){// i is odd
					int minCase2 = Integer.MAX_VALUE;
					for(Edge edge: inputWithIncomingEdges[vertex]){
						int case2Part1 = pParameterOfVertex[0][edge.otherNode];
						int case2 = (case2Part1 == Integer.MAX_VALUE)? case2Part1 : case2Part1 + edge.edgeLength;
						if(case2 < minCase2 ){
							minCase2 = case2;
						}
					}
					pParameterOfVertex[1][vertex] = Math.min(pParameterOfVertex[0][vertex], minCase2);
					
				}else{ // i is even
					int minCase2 = Integer.MAX_VALUE;
					for(Edge edge: inputWithIncomingEdges[vertex]){
						int case2Part1 = pParameterOfVertex[1][edge.otherNode];
						int case2 = (case2Part1 == Integer.MAX_VALUE)? case2Part1 : case2Part1 + edge.edgeLength;
						if(case2 < minCase2 ){
							minCase2 = case2;
						}
					}
					pParameterOfVertex[0][vertex] = Math.min(pParameterOfVertex[1][vertex], minCase2);
					
				}
				if(i == numOfNodes){ // checking for negative cycle
					if(pParameterOfVertex[1][vertex] != pParameterOfVertex[0][vertex]){
						negativeCycle = true;
						System.out.println("Negative cycle detected!!");
						break;
					}
				}
				
			}
		}
		return negativeCycle;
		
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		String file = "/Users/spoor/Desktop/Algorithms_assignment/Algorithms4/Week1/g3.txt";
		AllPairShortestPaths_Johnsons apsp = new AllPairShortestPaths_Johnsons(file);
		apsp.shortest_ShortestDistanceComputation();
		//g1.txt = negative cycle , g2.txt = negative cycle, g3.txt = -19
	}

}
